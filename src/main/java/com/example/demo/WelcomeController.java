package com.example.demo;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class WelcomeController {

	@GetMapping("/welcome")
	public String welcome() {
		return "Welcome to spring boot heroku demo";
	}
	
	@GetMapping("/hello")
	public String hello() {
		return "Hello to spring boot heroku demo";
	}
	
	@GetMapping("/")
	public String html() {
		String welcomePage = "<!DOCTYPE html> <html> <head> </head> <body> <h1 style=\"text-align:center;font-size:600%;\"> &nbsp </h1> <h1 style=\"text-align:center;font-size:700%;\">Welcome</h1> </body> </html>";
		return welcomePage;
	}
	
	@GetMapping("/test")
	public String test() {
		return "<!DOCTYPE html> <html> <head> </head> <body> <h1 style=\"text-align:center;font-size:600%;\"> &nbsp </h1> <h1 style=\"text-align:center;font-size:700%;\">TEST</h1> </body> </html>";
	}
	@GetMapping("/test1")
	public String test1() {
		return "<!DOCTYPE html> <html> <head> </head> <body> <h1 style=\"text-align:center;font-size:600%;\"> &nbsp </h1> <h1 style=\"text-align:center;font-size:700%;\">TEST1</h1> </body> </html>";
	}
	
	@GetMapping("/test2")
	public String test2() {
		return "<!DOCTYPE html> <html> <head> </head> <body> <h1 style=\"text-align:center;font-size:600%;\"> &nbsp </h1> <h1 style=\"text-align:center;font-size:700%;\">TEST2</h1> </body> </html>";
	}
	
	
}
